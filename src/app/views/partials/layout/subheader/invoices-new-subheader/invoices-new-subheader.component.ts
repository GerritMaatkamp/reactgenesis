import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 't2p-invoices-new-subheader',
  templateUrl: './invoices-new-subheader.component.html',
  styleUrls: ['./invoices-new-subheader.component.scss']
})
export class InvoicesNewSubheaderComponent implements OnInit {
  @Input() fluid: boolean;
  @Input() clear: boolean;
  @Input() chosenUserMethod: string;

  constructor() { }

  ngOnInit() {
  }
  

}
