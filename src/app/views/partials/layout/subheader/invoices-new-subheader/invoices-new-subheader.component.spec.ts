import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicesNewSubheaderComponent } from './invoices-new-subheader.component';

describe('InvoicesNewSubheaderComponent', () => {
  let component: InvoicesNewSubheaderComponent;
  let fixture: ComponentFixture<InvoicesNewSubheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicesNewSubheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicesNewSubheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
