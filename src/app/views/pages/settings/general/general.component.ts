import { Component, OnInit } from '@angular/core';

@Component({
  selector: 't2p-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})
export class GeneralComponent implements OnInit {
  company = {
    name: 'Dinobox',
    phone: '0612345678',
    email: 'm.bogaard@dibobox.nl',
    url: 'https://www.dinobox.nl'
  }


  constructor() { }

  ngOnInit() {
  }

}


