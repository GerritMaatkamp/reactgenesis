import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material'; 
import { log } from 'util';
import { FormGroup, FormBuilder } from '@angular/forms';



@Component({
  selector: 't2p-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.scss']
})
export class TemplatesComponent implements OnInit {
  emailTemplate: FormGroup = this.fb.group({
    SupplierEmailTemplateId: [0],
    SupplierId: [0],
    SupplierEmailTemplateCode: [''],
    Description: [''],
    EmailFrom: [''],
    EmailBcc: [''],
    EmailReplyTo: [''],
    EmailSubjectTemplate: [''],
    BodyTextTemplate: [''],
    BodyHtmlTemplate: [''],
    SupplierName: ['']
  });

  templateExamples = [
    {
      SupplierEmailTemplateId: [1111],
      SupplierId: [0],
      SupplierEmailTemplateCode: [1],
      Description: ['Default'],
      EmailFrom: ['random@email.com'],
      EmailBcc: [''],
      EmailReplyTo: ['random@email.com'],
      EmailSubjectTemplate: ['Your invoice'],
      BodyTextTemplate: [''],
      BodyHtmlTemplate: [''],
      SupplierName: ['Dinobox']
    },
    {
      SupplierEmailTemplateId: [1112],
      SupplierId: [0],
      SupplierEmailTemplateCode: [2],
      Description: ['Eerste herinnering'],
      EmailFrom: ['random@email.com'],
      EmailBcc: [''],
      EmailReplyTo: ['random@email.com'],
      EmailSubjectTemplate: ['Betatlingsherinnering'],
      BodyTextTemplate: [''],
      BodyHtmlTemplate: [''],
      SupplierName: ['Dinobox']
    },
    {
      SupplierEmailTemplateId: [1113],
      SupplierId: [0],
      SupplierEmailTemplateCode: [3],
      Description: ['Tweede herinnering'],
      EmailFrom: ['random@email.com'],
      EmailBcc: [''],
      EmailReplyTo: ['random@email.com'],
      EmailSubjectTemplate: ['Betalingsherinnering'],
      BodyTextTemplate: [''],
      BodyHtmlTemplate: [''],
      SupplierName: ['Dinobox']
    },
    {
      SupplierEmailTemplateId: [1113],
      SupplierId: [0],
      SupplierEmailTemplateCode: [4],
      Description: 'Eerste aanmaning',
      EmailFrom: ['random@email.com'],
      EmailBcc: [''],
      EmailReplyTo: ['random@email.com'],
      EmailSubjectTemplate: ['Betalingsherinnering'],
      BodyTextTemplate: [''],
      BodyHtmlTemplate: [''],
      SupplierName: ['Dinobox']
    },
  ]

  makeNewTemplate: boolean = false;
  previewTemplate: boolean = false;
  templateCode: number;
  templateName: string;
  thisTemplate: any;

  constructor(
    private _formBuilder: FormBuilder,
    private fb: FormBuilder, 
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    console.log('this.templateExamples');
    // console.log(this.templateExamples);
  }
  
  toggleNewTemplate() {
    this.previewTemplate = false;
    this.makeNewTemplate = !this.makeNewTemplate;
  }
  
  toggleThisTemplate(event) {
    this.previewTemplate = false;
    this.makeNewTemplate = !this.makeNewTemplate;
    console.log(event.target);
    let target = event.target;
    let target2 = event.srcElement;
    console.log(target2);
    
    // let idAttr = target.attributes.id;
    // console.log(idAttr);
    
  }

  newTemplate(){
    if (this.emailTemplate.dirty) {
      if (this.emailTemplate.invalid) {
        return;
      }
      this.templateExamples.push(
        {
          SupplierEmailTemplateId: this.emailTemplate.controls['SupplierEmailTemplateId'].value,
          SupplierId: [0],
          SupplierEmailTemplateCode: [this.templateExamples.length+1],
          Description: this.emailTemplate.controls['Description'].value,
          EmailFrom: this.emailTemplate.controls['EmailFrom'].value,
          EmailBcc: this.emailTemplate.controls['EmailBcc'].value,
          EmailReplyTo: this.emailTemplate.controls['EmailReplyTo'].value,
          EmailSubjectTemplate: this.emailTemplate.controls['EmailSubjectTemplate'].value,
          BodyTextTemplate: this.emailTemplate.controls['BodyTextTemplate'].value,
          BodyHtmlTemplate: this.emailTemplate.controls['BodyHtmlTemplate'].value,
          SupplierName: ['Dinobox']
        }
      ) 
    }
    console.log(this.templateExamples);
    this.emailTemplate = this.fb.group({
      SupplierEmailTemplateId: [0],
      SupplierId: [0],
      SupplierEmailTemplateCode: [''],
      Description: [''],
      EmailFrom: [''],
      EmailBcc: [''],
      EmailReplyTo: [''],
      EmailSubjectTemplate: [''],
      BodyTextTemplate: [''],
      BodyHtmlTemplate: [''],
      SupplierName: ['']
    });

    this.emailTemplate.markAsPristine();
    this.emailTemplate.markAsUntouched();
    this.toggleNewTemplate();
  }

  previewTemplateNow(template) {
    this.thisTemplate = template;
    this.makeNewTemplate = false;
    this.previewTemplate = true;
    console.log(this.thisTemplate);
    console.log(template.SupplierEmailTemplateCode);
    
  }
  
  openCorrespondenceDialog(templateCode, templateName) {
    this.templateCode = templateName
    this.templateName = templateCode
    const dialogRef = this.dialog.open(ArchivingDialog, {
      width: '300px',
      data: {
        Description: templateName,
        type: templateCode
      }
    });
  }

}

@Component({
  selector: 'archiving-dialog',
  templateUrl: 'templates.delete.html',
  styleUrls: ['./templates.component.scss']
})

export class ArchivingDialog {

  constructor(
    public dialogRef: MatDialogRef<ArchivingDialog>,
    @Inject(MAT_DIALOG_DATA) public data: SelectedTemplate) { }

  onNoClick(): void {
    this.dialogRef.close();
  }


  testThis() {
    console.log(this.data);
    console.log(this.data.type);
    console.log('/////////////');
  }
} 

export interface SelectedTemplate {
  Description: string,
  type: string,
  SupplierEmailTemplateCode: number
} 