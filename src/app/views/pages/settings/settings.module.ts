import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MAT_DATE_LOCALE, MatInputModule, MatFormFieldModule, MatDatepickerModule, MatAutocompleteModule, MatListModule, MatSliderModule, MatCardModule, MatSelectModule, MatButtonModule, MatIconModule, MatNativeDateModule, MatSlideToggleModule, MatCheckboxModule, MatMenuModule, MatTabsModule, MatTooltipModule, MatSidenavModule, MatProgressBarModule, MatProgressSpinnerModule, MatSnackBarModule, MatTableModule, MatGridListModule, MatToolbarModule, MatBottomSheetModule, MatExpansionModule, MatDividerModule, MatSortModule, MatStepperModule, MatChipsModule, MatPaginatorModule, MatDialogModule, MatRippleModule, MatRadioModule, MatTreeModule, MatButtonToggleModule } from '@angular/material';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { HttpClientModule } from '@angular/common/http';

import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';
import { GeneralComponent } from './general/general.component';
import { SettingsComponent } from './settings.component';
import { TranslateModule } from '@ngx-translate/core';
import { BrandComponent } from './brand/brand.component';
import { MaterialPreviewModule } from '../../partials/content/general/material-preview/material-preview.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MccColorPickerModule } from 'material-community-components';
import { TemplatesComponent, ArchivingDialog } from './templates/templates.component';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      {
        path: 'general',
        component: GeneralComponent
      },
      {
        path: 'brand',
        component: BrandComponent
      },
      {
        path: 'templates',
        component: TemplatesComponent
      },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    PartialsModule,
    CoreModule,
    MatInputModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatAutocompleteModule,
    MatListModule,
    MatSliderModule,
    MatCardModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatMenuModule,
    MatTabsModule,
    MatTooltipModule,
    MatSidenavModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatTableModule,
    MatGridListModule,
    MatToolbarModule,
    MatBottomSheetModule,
    MatExpansionModule,
    MatDividerModule,
    MatSortModule,
    MatStepperModule,
    MatChipsModule,
    MatPaginatorModule,
    MatDialogModule,
    MatRippleModule,
    CoreModule,
    CommonModule,
    MatRadioModule,
    MatTreeModule,
    MatButtonToggleModule,
    PartialsModule,
    MaterialPreviewModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    TranslateModule,
    MccColorPickerModule.forRoot({
      empty_color: 'transparent',
      used_colors: ['#000000', '#FFF555']
    }),
  ],
  exports: [RouterModule],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
  ],
  declarations: [
    SettingsComponent,
    GeneralComponent,
    BrandComponent,
    TemplatesComponent,
    ArchivingDialog,
  ],
  entryComponents: [
    SettingsComponent,
    GeneralComponent,
    BrandComponent,
    TemplatesComponent,
    ArchivingDialog
  ],
  bootstrap: [SettingsComponent]
})
export class SettingsModule { }
