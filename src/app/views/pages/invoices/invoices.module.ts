// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
// Core Module
import { CoreModule } from '../../../core/core.module';
import { PartialsModule } from '../../partials/partials.module';

import { InvoicesComponent } from './invoices.component';
import { ImportComponent } from './import/import.component';
import { NewComponent } from './new/new.component';
import { OverviewComponent, CorrespondenceDialog } from './overview/overview.component';
import { RemindersComponent } from './reminders/reminders.component';


import { HttpClientModule } from '@angular/common/http';
import { MAT_DATE_LOCALE, MatButtonToggleModule, MatRadioModule, MatTreeModule, MatRippleModule, MatDialogModule, MatPaginatorModule, MatChipsModule, MatStepperModule, MatSortModule, MatDividerModule, MatExpansionModule, MatBottomSheetModule, MatToolbarModule, MatGridListModule, MatTableModule, MatSnackBarModule, MatProgressSpinnerModule, MatProgressBarModule, MatSidenavModule, MatTooltipModule, MatTabsModule, MatMenuModule, MatCheckboxModule, MatSlideToggleModule, MatNativeDateModule, MatIconModule, MatButtonModule, MatSelectModule, MatCardModule, MatSliderModule, MatListModule, MatAutocompleteModule, MatDatepickerModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialPreviewModule } from '../../partials/content/general/material-preview/material-preview.module';
import { TranslateModule } from '@ngx-translate/core';


const routes: Routes = [
    {
        path: '',
        component: InvoicesComponent,
        children: [
            {
                path: 'overview',
                component: OverviewComponent
            },
            {
                path: 'new',
                component: NewComponent
            },
            {
                path: 'import',
                component: ImportComponent
            },
            {
                path: 'reminders', 
                component: ImportComponent
            },
        ]

    }
];

@NgModule({
    imports: [
        MatInputModule,
        MatFormFieldModule,
        MatDatepickerModule,
        MatAutocompleteModule,
        MatListModule,
        MatSliderModule,
        MatCardModule,
        MatSelectModule,
        MatButtonModule,
        MatIconModule,
        MatNativeDateModule,
        MatSlideToggleModule,
        MatCheckboxModule,
        MatMenuModule,
        MatTabsModule,
        MatTooltipModule,
        MatSidenavModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatSnackBarModule,
        MatTableModule,
        MatGridListModule,
        MatToolbarModule,
        MatBottomSheetModule,
        MatExpansionModule,
        MatDividerModule,
        MatSortModule,
        MatStepperModule,
        MatChipsModule,
        MatPaginatorModule,
        MatDialogModule,
        MatRippleModule,
        MatRadioModule,
        MatTreeModule,
        MatButtonToggleModule,
        MaterialPreviewModule,
        CoreModule,
        CommonModule,
        PartialsModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        TranslateModule 
    ],
    exports: [RouterModule],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
        { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    ],
    declarations: [
        InvoicesComponent,
        ImportComponent,
        NewComponent,
        OverviewComponent,
        RemindersComponent,
        CorrespondenceDialog, 
        OverviewComponent
    ],
    entryComponents: [
        InvoicesComponent, 
        CorrespondenceDialog, 
        OverviewComponent
    ],
    bootstrap: [InvoicesComponent]
})
export class InvoicesModule {
}
