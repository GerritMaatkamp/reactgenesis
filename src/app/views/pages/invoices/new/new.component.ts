import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_TOOLTIP_DEFAULT_OPTIONS, MatTooltipDefaultOptions } from '@angular/material'
import { UserDetails } from '../../../models/user-details';
import { SupplierInvoice } from '../../../models/supplier-invoice';
import { DatePipe } from '@angular/common';
import * as moment from 'moment'; // add this 1 of 4
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'kt-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit {
  now: Date = new Date();
  thisMoment = moment();
  chosenUserMethod: string = 'none';
  userForm: FormGroup = this.fb.group({
    DebtorFirstName: [''],
    DebtorLastName: [''],
    DebtorEmail: [''],
    DebtorPhoneNumber: [''],
    DebtorStreetname: [''],
    DebtorHouseNumber: [0],
    DebtorHouseNumberExtension: [''],
    DebtorZipCode: [''],
    DebtorCity: [''],
    DebtorCountry: ['']
  });
  invoiceForm: FormGroup = this.fb.group({
    SupplierCustomerId: [''],
    SupplierKeyValue: [],
    DebtorDetails: {
      DebtorName: [''],
      DebtorStreetname: [''],
      DebtorHouseNumber: [0],
      DebtorHouseNumberExtension: [''],
      DebtorZipCode: [''],
      DebtorCity: [''],
      DebtorCountry: [''],
      DebtorEmail: [''],
      DebtorPhoneNumber: [''],
      DinoboxUserId: [0],
      LogDinoboxUserId: [0],
    },
    InvoiceNumber: [''],
    PurchaseId: [''],
    InvoiceStateId: [0],
    Description: [''],
    ExtendedDescription: [''],
    Amount: [0],
    InvoiceDate: moment().add(0, 'months').toDate(),
    InvoicePaymentTerm: [14],
    InvoiceDueDate: [null],

    InstallmentPlanDueByUtc: [null],
    InstallmentPlanStartByUtc: [null],
    InstallmentPlanNumberOfPayments: [0],
    InstallmentPlanPaymentAmount: [0],
    InstallmentPlanInterval: [''],

    MandateIdentification: [''],
    CreditorIdentification: [''],

    ConsumerBic: [''],
    ConsumerIban: [''],
    DirectDebitInterval: [''],
    DirectDebitEndDate: [null],

    EmailScheduledSendUtc: [''],
    SupplierExtraDownloadUrl: [''],
    ContractName: [''],
    ClaimCosts: [0],
    InterestCosts: [0],
    ReminderAmountPaid: [0],
    LastClaimStepDate: [null]
  });
  position = 'above';
  advancedSettings: boolean = false;
  countriesControl: FormControl = new FormControl();
  user: string = "none" ;
  startInvoice: boolean = false;

  exampleUser: UserDetails = new UserDetails;

  exampleInvoice: SupplierInvoice = new SupplierInvoice;

  paymentPlan: boolean = false;
  directDebit: boolean = false;
  eMandate: boolean = false;
  interest: boolean = false;

  intervals = ['Weekly', 'Bi-weekly','Monthly','Yearly'];

  paymentPlanTerms: number;
  paymentPlanAmount: number;
  InvoiceAmount: number;
  dueDate: any;
  dateInput: moment.Moment;
  paymentTerm: any;
  invoiceOverview: boolean = false;

  fakecompany = {
    name: 'Logo Design B.V.',
    street: 'Coolsingel',
    housenumber: '40',
    postalcode: '3011 AD.',
    city: 'Rotterdam',
    kvk: '12345678',
    btw: 'NL001234567B01',
    bank: 'NL69INGB0123456789',
  }
  fakecustomer = {
    DebtorfirstName: 'John',
    DebtorlastName: 'Diggle',
    Debtoremail: 'johndiggle@queenconsolidated.com',
    DebtorPhoneNumber: '(908)-463-8484',
    DebtorStreetname: '(908)-463-8484',
    DebtorHouseNumber: 3,
    DebtorHouseNumberExtension: 'null',
    DebtorZipCode: '30307',
    DebtorCity: 'Starling City',
    DebtorCountry: 'the United States'
  }

  @Output() change = new EventEmitter();

  // translation
  // form 1 - user
  // firstName: string = this.translate.instant('NEWINVOICE.NEWUSERFORM.FIRSTNAME');
  // lastName: string = this.translate.instant('NEWINVOICE.NEWUSERFORM.LASTNAME');
  // eMail: string = this.translate.instant('NEWINVOICE.NEWUSERFORM.EMAILPLACEHOLDER');
  // mobilePhone: string = this.translate.instant('NEWINVOICE.NEWUSERFORM.MOBILEPHONEPLACEHOLDER');
  // street: string = this.translate.instant('NEWINVOICE.NEWUSERFORM.ADDRESSPLACEHOLDER');
  // houseNumber: string = this.translate.instant('NEWINVOICE.NEWUSERFORM.HOUSENUMBERPLACEHOLDER');
  // extension: string = this.translate.instant('NEWINVOICE.NEWUSERFORM.EXTENSION');
  // zip: string = this.translate.instant('NEWINVOICE.NEWUSERFORM.ZIPPLACEHOLDER');
  // city: string = this.translate.instant('NEWINVOICE.NEWUSERFORM.CITYPLACEHOLDER');
  // country: string = this.translate.instant('NEWINVOICE.NEWUSERFORM.COUNTRY');

  constructor(
    private _formBuilder: FormBuilder, 
    private fb: FormBuilder, 
    private datePipe: DatePipe,
    private translate: TranslateService,
    ) { }

  ngOnInit() {
    this.generateInvoiceNumber(1);
    this.createDueDate();
    console.log('this.fakecompany');
    console.log(this.fakecompany);
    
  }
  // breadcrumb toggles
  togglePickUser() {
    this.chosenUserMethod = 'none';
    this.startInvoice = false;
    this.invoiceOverview = false;
  }
  
  toggleMakeUser() {
    this.startInvoice = !this.startInvoice;
    this.invoiceOverview = false;
  }

  toggleMakeInvoice() {
    this.invoiceOverview = !this.invoiceOverview;
  }


  newUser(){
    this.chosenUserMethod = 'New user';
    this.change.emit(this.chosenUserMethod);
  }

  userFromDB(){
    this.chosenUserMethod = 'User from database';
  }

  togglePaymentPlan() {
    this.paymentPlan = !this.paymentPlan;
    console.log(this.paymentPlan);
  }

  toggleDirectDebit() {
    this.directDebit = !this.directDebit;
    console.log(this.directDebit);
  }

  toggleEmandate() {
    this.eMandate = !this.eMandate;
    console.log(this.eMandate);
    
  }
  toggleInterestAndClaim() {
    this.interest = !this.interest;
    console.log(this.interest);
  }

  generateInvoiceNumber(n) {
    var year   = new Date().getFullYear().toString();
    var month  = new Date().getMonth().toString();
    var count = n;
    this.exampleInvoice.InvoiceNumber = year + month + "0" + count.toString(); 
  }

  createDueDate() {
    this.dateInput = moment(this.invoiceForm.controls.InvoiceDate.value);
    this.paymentTerm = this.invoiceForm.controls.InvoicePaymentTerm.value;
    this.dueDate = this.dateInput.clone().add(this.paymentTerm, 'day');  
  }

  saveUser() {
    if (this.userForm.dirty) {
      if (this.invoiceForm.invalid) {
        return;
      }
      this.exampleUser.DebtorFirstName = this.userForm.controls.DebtorFirstName.value;
      this.exampleUser.DebtorLastName = this.userForm.controls.DebtorLastName.value;
      this.exampleUser.DebtorEmail = this.userForm.controls.DebtorEmail.value;
      this.exampleUser.DebtorPhoneNumber = this.userForm.controls.DebtorPhoneNumber.value;
      this.exampleUser.DebtorStreetname = this.userForm.controls.DebtorStreetname.value;
      this.exampleUser.DebtorHouseNumber = this.userForm.controls.DebtorHouseNumber.value;
      this.exampleUser.DebtorHouseNumberExtension = this.userForm.controls.DebtorHouseNumberExtension.value;
      this.exampleUser.DebtorZipCode = this.userForm.controls.DebtorZipCode.value;
      this.exampleUser.DebtorCity = this.userForm.controls.DebtorCity.value;
      this.exampleUser.DebtorCountry = this.userForm.controls.DebtorCountry.value;
    }
    console.log(this.exampleUser);
    this.startInvoice = true;
  }

  calculatePaymentPlanAmount() {
    this.paymentPlanTerms = parseFloat(this.invoiceForm.controls.InstallmentPlanNumberOfPayments.value);
    if (this.paymentPlanTerms > 0 && this.InvoiceAmount > 0 ) {
      this.paymentPlanAmount = this.InvoiceAmount / this.paymentPlanTerms;
      
    }
  }

  calculatePaymentPlanTerms() {
    this.paymentPlanAmount = parseFloat(this.invoiceForm.controls.InstallmentPlanPaymentAmount.value);
    if (this.paymentPlanAmount > 0 && this.InvoiceAmount > 0){
      this.paymentPlanTerms = this.InvoiceAmount / this.paymentPlanAmount;
    }
  }

  saveInvoice() {
    if (this.invoiceForm.dirty) {
      if (this.invoiceForm.invalid) {
        return;
      }
      this.generateInvoiceNumber(1);

      this.exampleInvoice.Description = this.invoiceForm.controls.Description.value;
      this.exampleInvoice.Amount = this.invoiceForm.controls.Amount.value;
      this.exampleInvoice.InvoiceDate = this.invoiceForm.controls.InvoiceDate.value;
      this.exampleInvoice.InvoicePaymentTerm = this.invoiceForm.controls.InvoicePaymentTerm.value;
      this.exampleInvoice.InvoiceDueDate = this.dueDate;
      
      // advanced settings
      // DD
      this.exampleInvoice.ConsumerIban = this.invoiceForm.controls.ConsumerIban.value;
      this.exampleInvoice.ConsumerBic = this.invoiceForm.controls.ConsumerBic.value;
      this.exampleInvoice.DirectDebitInterval = this.invoiceForm.controls.DirectDebitInterval.value;
      this.exampleInvoice.DirectDebitEndDate = this.invoiceForm.controls.DirectDebitEndDate.value;
      // PP
      this.exampleInvoice.InstallmentPlanNumberOfPayments = this.invoiceForm.controls.InstallmentPlanNumberOfPayments.value;
      this.exampleInvoice.InstallmentPlanPaymentAmount = this.invoiceForm.controls.InstallmentPlanPaymentAmount.value;
      this.exampleInvoice.InstallmentPlanInterval = this.invoiceForm.controls.InstallmentPlanInterval.value;
      this.exampleInvoice.InstallmentPlanStartByUtc = this.invoiceForm.controls.InstallmentPlanStartByUtc.value;
      // this.exampleInvoice.InstallmentPlanDueByUtc = this.invoiceForm.controls.InstallmentPlanDueByUtc.value;
      
      // EM
      this.exampleInvoice.CreditorIdentification = this.invoiceForm.controls.CreditorIdentification.value;
      this.exampleInvoice.MandateIdentification = this.invoiceForm.controls.MandateIdentification.value;
      // CA
      this.exampleInvoice.ContractName = this.invoiceForm.controls.ContractName.value;
      this.exampleInvoice.InterestCosts = this.invoiceForm.controls.InterestCosts.value;
      this.exampleInvoice.ClaimCosts = this.invoiceForm.controls.ClaimCosts.value;
      console.log(this.exampleInvoice);

    }
  }
}