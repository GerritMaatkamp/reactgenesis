import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'kt-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InvoicesComponent implements OnInit {

  constructor(
    // private iconRegistry: MatIconRegistry,
    // private sanitizer: DomSanitizer
     ) {
    //     this.setupIconRegistry(iconRegistry, sanitizer);
      }
  
  ngOnInit() {

  }

  // setupIconRegistry(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
  //   // general
  //   iconRegistry.addSvgIcon('icon_settings', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_cog.svg'));
  //   iconRegistry.addSvgIcon('icon_dashboard', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_dashboard.svg'));
  //   iconRegistry.addSvgIcon('icon_invoice_euro', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_invoice_euro.svg'));
  //   iconRegistry.addSvgIcon('icon_edit', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_edit.svg'));
  //   iconRegistry.addSvgIcon('icon_envelope', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_envelope.svg'));
    
  //   // overview
  //   iconRegistry.addSvgIcon('icon_envelope_bold', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_envelope_bold.svg'));
  //   iconRegistry.addSvgIcon('icon_preview_bold', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_preview_bold.svg'));
  //   iconRegistry.addSvgIcon('icon_edit_bold', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_edit_bold.svg'));
    
  //   // make invoice
  //   iconRegistry.addSvgIcon('icon_userDB', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_userDB.svg'));
  //   iconRegistry.addSvgIcon('icon_userAdd', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_userAdd.svg'));

  //   iconRegistry.addSvgIcon('icon_xls', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_xls.svg'));
  //   iconRegistry.addSvgIcon('icon_pdf', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_pdf.svg'));
  //   iconRegistry.addSvgIcon('icon_worddoc', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_worddoc.svg'));
  // }
}
