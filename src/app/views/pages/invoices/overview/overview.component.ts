import { Component, ViewChild, OnInit, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { DatePipe } from '@angular/common';
import { FormControl, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'kt-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  displayedColumns: string[] = ['select','id','supplier', 'invoicenumber', 'date', 'customerid', 'name',
    'zip', 'city', 'amount', 'currency', 'state'];
  color: string;
  invoices: MatTableDataSource<SupplierInvoice>;
  selection = new SelectionModel<SupplierInvoice>(true, []);
  pipe: DatePipe;
  templateCode: number;
  templateName: string;
  supplierEmailTemplates = [
    {Description: 'Default', type: 'invoice', SupplierEmailTemplateCode: 1234}, 
    {Description: 'First reminder', type: 'reminder', SupplierEmailTemplateCode: 1235},
    {Description: 'Second reminder', type: 'reminder', SupplierEmailTemplateCode: 1236} 
  ];

  filterForm = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
  });
  SupplierName = this.translate.instant('GENERAL.SUPPLIERNAME');
  InvoiceNumber = this.translate.instant('GENERAL.INVOICENUMBER');
  InvoiceDate   = this.translate.instant('GENERAL.INVOICEDATE');
  CustomerID = this.translate.instant('GENERAL.CUSTOMERID');
  CustomerName = this.translate.instant('GENERAL.CUSTOMERNAME');
  PostalCode = this.translate.instant('GENERAL.Postcode');
  City = this.translate.instant('GENERAL.CITY');
  Currency = this.translate.instant('GENERAL.CURRENCY');
  Status = this.translate.instant('GENERAL.STATUS');

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  ngOnInit() {
    this.invoices.sort = this.sort;
  }

  constructor(public dialog: MatDialog, private translate: TranslateService,) {
    // Create 100 users
    const users: SupplierInvoice[] = [];
    for (let i = 1; i <= 100; i++) { users.push(createNewUser(i)); }

    // Assign the data to the data source for the table to render
    this.invoices = new MatTableDataSource(users);
  }
  
  applyDateFilter() {
    this.invoices.filter = '' + Math.random();
  }
  /**
  * Set the paginator and sort after the view init since this component will
  * be able to query its view for the initialized paginator and sort.
  */
  ngAfterViewInit() {
    this.invoices.paginator = this.paginator;
    this.invoices.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.invoices.filter = filterValue;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.invoices.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    // check of in this.invoices het resultaat na filteren te zien is (kan ik zien wat de 24 zijn)
    this.isAllSelected() ?
      this.selection.clear() :
      this.invoices.data.forEach(row => this.selection.select(row));
  }

  checkboxLabel(row?: SupplierInvoice): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  openCorrespondenceDialog(templateCode, templateName) {
    let geselecteerdeInvoices = this.invoices.data.filter(i => i.Selected);
    this.templateCode = templateName
    this.templateName = templateCode
    const dialogRef = this.dialog.open(CorrespondenceDialog, {
      width: '300px',
      data: {
        templateName: templateName,
        templateCode: templateCode,
        amount: this.selection.selected
      }
    });
  }


  
}

@Component({
  selector: 'correspondence-dialog',
  templateUrl: 'correspondence.dialog.html',
  styleUrls: ['./overview.component.scss']
})
export class CorrespondenceDialog {
  sendingCorrespondence: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<CorrespondenceDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  sendCorrespondence() {
    this.toggleSend();
    // functionality to actually send correspondence goes here
    // 
    // 
    // functionality to actually send correspondence goes here
  }


  toggleSend() {
    this.sendingCorrespondence = !this.sendingCorrespondence;
    

  }
}


// fakes database
// fakes database
// fakes database

/** Builds and returns a new User. */
function createNewUser(id: number): SupplierInvoice {
  const name =
    NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
    LASTNAMES[Math.round(Math.random() * (NAMES.length - 1))];

  const randomInvoiceNumber = Math.round(Math.random()*1000000);
  const randomID = Math.round(Math.random()*1000000);
  
  function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  }

  return {
    position: 0, 
    Selected: false,
    id: id.toString(),
    supplier: SUPPLIERS[Math.round(Math.random() * (SUPPLIERS.length - 1))],
    invoicenumber: randomInvoiceNumber,
    date: randomDate(new Date(2019, 0, 1), new Date()),
    customerid: 'SCID' + '-' + randomID,
    name: name,
    zip: 'x',
    city: 'x',
    amount: (Math.random() * 1000).toFixed(2),
    currency: 'EUR',
    state: STATE[Math.round(Math.random() * (STATE.length - 1))],
  };
}

/** Constants used to fill up our data base. */
const NAMES = ['Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack',
  'Charlotte', 'Theodore', 'Isla', 'Oliver', 'Isabella', 'Jasper',
  'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'];
const LASTNAMES = ['van Dijk', 'Bogaard', 'Ouwens', 'de Bever', 'Tokkie', 'Spaans',
  'Ledalay', 'Amin', 'Kökcü', 'Bakker', 'Wever', 'Verstegen',
  'Beers', 'Hebing', 'Ledemans', 'Vermeer', 'Lens', 'Conway', 'Havel'];  
const SUPPLIERS = ['mijnAndersnota.nl', 'DirectPay', 'Dinobox','Webcasso', 'WebCasso (2)'];
const STATE = ['New','Read','Payment started','Paid','Soft Bounce','Hard Bounce','Complaint','Payment plan'];

export interface DialogData { 
  Description: string, 
  type: string, 
  SupplierEmailTemplateCode: number 
} 

export interface SupplierInvoice {
  id: string;
  position: number;
  supplier: string; 
  invoicenumber: number;
  date: any;
  customerid: string;
  name: string;
  zip: string;
  city: string;
  amount: string;
  currency: string;
  state: string;
  Selected: boolean;
}