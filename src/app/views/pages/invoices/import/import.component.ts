import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import * as XLSX from 'xlsx';
import * as moment from 'moment';
// import { SupplierInvoiceImport } from '@/app/views/models/supplier-invoice-import';
import { SupplierInvoiceImport } from '../../../models/supplier-invoice-import';
import { MatTableDataSource, MatSort } from '@angular/material';

@Component({
  selector: 'kt-import',
  templateUrl: './import.component.html',
  styleUrls: ['./import.component.scss']
})
export class ImportComponent implements OnInit {
  invoices = new MatTableDataSource<SupplierInvoiceImport>();
  excelFileName: string;
  selectedSupplierId: number;
  columns = ['InvoiceNumber', 'InvoiceDate', 'InvoiceDueDate', 'DebtorEmail', 'DebtorName', 'Description', 'Amount', 'SupplierCustomerId', 'SupplierName', 'PurchaseId', 'InstallmentPlanNumberOfPayments', 'InstallmentPlanPaymentAmount', 'importStatus'];
  // columns = ['invoiceNumber', 'invoiceDate', 'invoiceDueDate', 'email', 'name',
  //   'description', 'amount', 'supplierCustomerId', 'supplierName', 'purchaseId', 'installmentPlanNumberOfPayments', 'installmentPlanPaymentAmount', 'importStatus'];

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  invoicesPresent: boolean = false;

  constructor(
    private changeDetectorRefs: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.invoices.sort = this.sort;
  }

  onFileChange(event: any) {
    
    const target: DataTransfer = <DataTransfer>(event.target);

    if (target.files.length !== 1) {
      throw new Error('Cannot use multiple files');
    }
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary', cellDates: true });

      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      const range = XLSX.utils.decode_range(ws['!ref']);
      range.s.r = 1; // <-- zero-indexed, so setting to 1 will skip row 0
      ws['!ref'] = XLSX.utils.encode_range(range);

      const data = XLSX.utils.sheet_to_json(ws, { header: 1, defval: null });
        const counter = 1;

        let readInvoices: SupplierInvoiceImport[] = [];

      data.forEach(element => {

        if (element[0] !== null && element[0] !== undefined) {
          const newInvoice: SupplierInvoiceImport = new SupplierInvoiceImport();

          newInvoice.InvoiceId = 0;
          newInvoice.InvoiceNumber = element[0];

          // XSLX return 1-1-2019 as Mon Dec 31 2018 23:59:28 GMT+0100 (Midden-Europese standaardtijd)
          newInvoice.InvoiceDate = moment(element[1]).add(1, 'hours').toDate();
          newInvoice.InvoiceDueDate = moment(element[2]).add(1, 'hours').toDate();
          // debtor details
          newInvoice.DebtorEmail = element[3];
          newInvoice.DebtorName = element[4];
          newInvoice.DebtorZipCode = element[5];
          newInvoice.DebtorHouseNumber = element[6];
          newInvoice.DebtorHouseNumberExtension = element[7];
          newInvoice.DebtorStreetname = element[8];
          newInvoice.DebtorCity = element[9];
          newInvoice.DebtorCountry = element[10];
          
          newInvoice.Description = element[11];
          newInvoice.Amount = element[12];
          newInvoice.Currency = 'EUR';
          newInvoice.SupplierCustomerId = element[13];
          newInvoice.SupplierName = element[14];
          newInvoice.PurchaseId = element[15];
          newInvoice.InstallmentPlanNumberOfPayments = element[16];
          newInvoice.InstallmentPlanPaymentAmount = element[17];
          newInvoice.importStatus = '';

          newInvoice.SupplierId = this.selectedSupplierId;
          readInvoices.push(newInvoice);
          this.changeDetectorRefs.detectChanges();
          this.invoicesPresent = true;
        }
      });
      this.invoices.data = readInvoices;
      this.invoices.sort = this.sort;
    };
    reader.readAsBinaryString(target.files[0]);
    this.excelFileName = target.files[0].name;
  }

}
