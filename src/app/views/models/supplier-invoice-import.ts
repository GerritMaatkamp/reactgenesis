import { SafeUrl } from '@angular/platform-browser';
import { SupplierInvoiceDBX } from './supplier-invoice-dbx';

export class SupplierInvoiceImport extends SupplierInvoiceDBX {
  constructor(jsonData?: any) {
    super(jsonData);

    this.importStatus = 'None';
  }

  importStatus: string;

}
