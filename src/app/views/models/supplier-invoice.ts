import { SafeUrl } from '@angular/platform-browser';

export class SupplierInvoice {
  constructor( jsonData?: any ) {

    if ( jsonData ) {
      this.DebtorDetails.DebtorFirstName = jsonData.DebtorFirstName;
      this.DebtorDetails.DebtorLastName = jsonData.DebtorLastName;
      this.DebtorDetails.DebtorStreetname = jsonData.DebtorStreetname;
      this.DebtorDetails.DebtorHouseNumber = jsonData.DebtorHouseNumber;
      this.DebtorDetails.DebtorHouseNumberExtension = jsonData.DebtorHouseNumberExtension;
      this.DebtorDetails.DebtorZipCode = jsonData.DebtorZipCode;
      this.DebtorDetails.DebtorCity = jsonData.DebtorCity;
      this.DebtorDetails.DebtorCountry = jsonData.DebtorCountry;
      this.DebtorDetails.DebtorEmail = jsonData.DebtorEmail;
      this.DebtorDetails.DebtorPhoneNumber = jsonData.DebtorPhoneNumber;
      this.DebtorDetails.DinoboxUserId = jsonData.DinoboxUserId;
      this.DebtorDetails.LogDinoboxUserId = jsonData.LogDinoboxUserId;

      this.SelectionSet = false;
      this.Selected = false;
      this.InvoiceId = jsonData.InvoiceId;
      this.SupplierId = jsonData.SupplierId;
      this.SupplierCustomerId = jsonData.SupplierCustomerId;
      this.InvoiceNumber = jsonData.InvoiceNumber;
      this.Description = jsonData.Description;
      this.InvoiceStateId = jsonData.InvoiceStateId;
      this.Amount = jsonData.Amount;
      this.Currency = jsonData.Currency;
      this.InvoiceDate = jsonData.InvoiceDate;
      this.InvoicePaymentTerm = jsonData.InvoicePaymentTerm;
      this.InvoiceDueDate = jsonData.InvoiceDueDate;
      this.InvoicePaidDateUtc = jsonData.InvoicePaidDateUtc;
      this.PDFData = jsonData.PDFData;
      this.PaymentIdealUrl = jsonData.PaymentIdealUrl;
      this.CallbackUrl = jsonData.CallbackUrl;
      this.SupplierKeyValue = jsonData.SupplierKeyValue;
      this.InvoiceStateDescription = jsonData.InvoiceStateDescription;
      this.SupplierName = jsonData.SupplierName;

      this.InstallmentPlanDueByUtc = jsonData.InstallmentPlanDueByUtc;
      this.InstallmentPlanNumberOfPayments = jsonData.InstallmentPlanNumberOfPayments;
      this.InstallmentPlanPaymentAmount = jsonData.InstallmentPlanPaymentAmount;
      this.InstallmentPlanInterval = jsonData.InstallmentPlanInterval;
      
      this.PurchaseId = jsonData.PurchaseId;
      this.MandateIdentification = jsonData.MandateIdentification;
      this.CreditorIdentification = jsonData.CreditorIdentification;
      
      this.ConsumerBic = jsonData.ConsumerBic;
      this.ConsumerIban = jsonData.ConsumerIban;
      this.DirectDebitInterval = jsonData.DirectDebitInterval;
      this.DirectDebitEndDate = jsonData.DirectDebitEndDate;

      this.EmailScheduledSendUtc = jsonData.EmailScheduledSendUtc;
      this.SupplierExtraDownloadUrl = jsonData.SupplierExtraDownloadUrl;
      this.SupplierRemarks = jsonData.SupplierRemarks;
      this.ContractName = jsonData.ContractName;
      this.ClaimCosts = jsonData.ClaimCosts;
      this.InterestCosts = jsonData.InterestCosts;
      this.ReminderAmountPaid = jsonData.ReminderAmountPaid;
      this.LastClaimStepDate = jsonData.LastClaimStepDate;
      this.RedirectUserAfterPaymentUrl = jsonData.RedirectUserAfterPaymentUrl;
      this.LogId = jsonData.LogId;
      this.LogTimestampUtc = jsonData.LogTimestampUtc;
      this.LogAction = jsonData.LogAction;
      this.ExtendedDescription = jsonData.ExtendedDescription;
    }
  }
  
  DebtorDetails: {
    DebtorFirstName: string,
    DebtorLastName: string,
    DebtorStreetname: string,
    DebtorHouseNumber: number,
    DebtorHouseNumberExtension: string,
    DebtorZipCode: string,
    DebtorCity: string,
    DebtorCountry: string,
    DebtorEmail: string,
    DebtorPhoneNumber: string,
    DinoboxUserId: number,
    LogDinoboxUserId: number,
  }

  SelectionSet: boolean;
  Selected: boolean;
  InvoiceId: number;
  SupplierId: number;
  SupplierCustomerId: string;
  InvoiceNumber: string;
  Description: string;
  InvoiceStateId: number;
  Amount: number;
  Currency: string;
  InvoiceDate: Date;
  InvoicePaymentTerm: number;
  InvoiceDueDate: Date;
  InvoicePaidDateUtc: Date;
  PDFData: any;
  PaymentIdealUrl: string;
  CallbackUrl: string;
  SupplierKeyValue: string;
  InvoiceStateDescription: string;
  SupplierName: string;

  InstallmentPlanDueByUtc: Date;
  InstallmentPlanStartByUtc: Date;
  InstallmentPlanNumberOfPayments: number;
  InstallmentPlanPaymentAmount: number;
  InstallmentPlanInterval: number;

  PurchaseId: string;
  MandateIdentification: string;
  CreditorIdentification: string;

  ConsumerBic: string;
  ConsumerIban: string;
  DirectDebitInterval: Date;
  DirectDebitEndDate: Date;

  EmailScheduledSendUtc: Date;
  SupplierExtraDownloadUrl: string;
  SupplierRemarks: string;
  ContractName: string;
  ClaimCosts: number;
  InterestCosts: number;
  ReminderAmountPaid: number;
  LastClaimStepDate: Date;
  RedirectUserAfterPaymentUrl: string;
  LogId: number;
  LogTimestampUtc: Date;
  LogAction: string;
  ExtendedDescription: string;

  canBePaid() {
    return ( this.InvoiceStateId !== 5 );
  }

  hasBeenPaid() {
    return ( this.InvoiceStateId === 5 );
  }
}
