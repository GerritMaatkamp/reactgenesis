import { SafeUrl } from '@angular/platform-browser';

export class SupplierInvoiceDBX {
    constructor(jsonData?: any) {

        if (jsonData) {
            this.SelectionSet = false;
            this.Selected = false;
            this.InvoiceId = jsonData.InvoiceId;
            this.SupplierId = jsonData.SupplierId;
            this.SupplierCustomerId = jsonData.SupplierCustomerId;
            this.InvoiceNumber = jsonData.InvoiceNumber;
            this.Description = jsonData.Description;
            this.InvoiceStateId = jsonData.InvoiceStateId;
            this.Amount = jsonData.Amount;
            this.Currency = jsonData.Currency;
            this.InvoiceDate = jsonData.InvoiceDate;
            this.InvoiceDueDate = jsonData.InvoiceDueDate;
            this.InvoicePaidDateUtc = jsonData.InvoicePaidDateUtc;
            this.PDFData = jsonData.PDFData;
            this.DebtorName = jsonData.DebtorName;
            this.DebtorStreetname = jsonData.DebtorStreetname;
            this.DebtorHouseNumber = jsonData.DebtorHouseNumber;
            this.DebtorHouseNumberExtension = jsonData.DebtorHouseNumberExtension;
            this.DebtorZipCode = jsonData.DebtorZipCode;
            this.DebtorCity = jsonData.DebtorCity;
            this.DebtorCountry = jsonData.DebtorCountry;
            this.DebtorEmail = jsonData.DebtorEmail;
            this.PaymentIdealUrl = jsonData.PaymentIdealUrl;
            this.CallbackUrl = jsonData.CallbackUrl;
            this.SupplierKeyValue = jsonData.SupplierKeyValue;
            this.InvoiceStateDescription = jsonData.InvoiceStateDescription;
            this.SupplierName = jsonData.SupplierName;
            this.InstallmentPlanDueByUtc = jsonData.InstallmentPlanDueByUtc;
            this.InstallmentPlanNumberOfPayments = jsonData.InstallmentPlanNumberOfPayments;
            this.InstallmentPlanPaymentAmount = jsonData.InstallmentPlanPaymentAmount;
            this.PurchaseId = jsonData.PurchaseId;
            this.MandateIdentification = jsonData.MandateIdentification;
            this.CreditorIdentification = jsonData.CreditorIdentification;
            this.ConsumerBic = jsonData.ConsumerBic;
            this.ConsumerIban = jsonData.ConsumerIban;
            this.EmailScheduledSendUtc = jsonData.EmailScheduledSendUtc;
            this.SupplierExtraDownloadUrl = jsonData.SupplierExtraDownloadUrl;
            this.SupplierRemarks = jsonData.SupplierRemarks;
            this.ContractName = jsonData.ContractName;
            this.ClaimCosts = jsonData.ClaimCosts;
            this.InterestCosts = jsonData.InterestCosts;
            this.ReminderAmountPaid = jsonData.ReminderAmountPaid;
            this.LastClaimStepDate = jsonData.LastClaimStepDate;
            this.RedirectUserAfterPaymentUrl = jsonData.RedirectUserAfterPaymentUrl;
            this.DinoboxUserId = jsonData.DinoboxUserId;
            this.LogDinoboxUserId = jsonData.LogDinoboxUserId;
            this.LogId = jsonData.LogId;
            this.LogTimestampUtc = jsonData.LogTimestampUtc;
            this.LogAction = jsonData.LogAction;
            this.ExtendedDescription = jsonData.ExtendedDescription;
            this.DebtorPhoneNumber = jsonData.DebtorPhoneNumber;
        }
    }

    SelectionSet: boolean;
    Selected: boolean;
    InvoiceId: number;
    SupplierId: number;
    SupplierCustomerId: string;
    InvoiceNumber: string;
    Description: string;
    InvoiceStateId: number;
    Amount: number;
    Currency: string;
    InvoiceDate: Date;
    InvoiceDueDate: Date;
    InvoicePaidDateUtc: Date;
    PDFData: any;
    DebtorName: string;
    DebtorStreetname: string;
    DebtorHouseNumber: number;
    DebtorHouseNumberExtension: string;
    DebtorZipCode: string;
    DebtorCity: string;
    DebtorCountry: string;
    DebtorEmail: string;
    PaymentIdealUrl: string;
    CallbackUrl: string;
    SupplierKeyValue: string;
    InvoiceStateDescription: string;
    SupplierName: string;
    InstallmentPlanDueByUtc: Date;
    InstallmentPlanNumberOfPayments: number;
    InstallmentPlanPaymentAmount: number;
    PurchaseId: string;
    MandateIdentification: string;
    CreditorIdentification: string;
    ConsumerBic: string;
    ConsumerIban: string;
    EmailScheduledSendUtc: Date;
    SupplierExtraDownloadUrl: string;
    SupplierRemarks: string;
    ContractName: string;
    ClaimCosts: number;
    InterestCosts: number;
    ReminderAmountPaid: number;
    LastClaimStepDate: Date;
    RedirectUserAfterPaymentUrl: string;
    DinoboxUserId: number;
    LogDinoboxUserId: number;
    LogId: number;
    LogTimestampUtc: Date;
    LogAction: string;
    ExtendedDescription: string;
    DebtorPhoneNumber: string;

    canBePaid() {
        return (this.InvoiceStateId !== 5);
    }

    hasBeenPaid() {
        return (this.InvoiceStateId === 5);
    }
}
