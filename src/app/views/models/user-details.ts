import { SafeUrl } from '@angular/platform-browser';

export class UserDetails {
    constructor(jsonData?: any) {

        if (jsonData) {
            this.SupplierCustomerId = jsonData.SupplierCustomerId;
            this.DebtorFirstName = jsonData.DebtorFirstName;
            this.DebtorLastName = jsonData.DebtorFirstName;
            this.DebtorStreetname = jsonData.DebtorStreetname;
            this.DebtorHouseNumber = jsonData.DebtorHouseNumber;
            this.DebtorHouseNumberExtension = jsonData.DebtorHouseNumberExtension;
            this.DebtorZipCode = jsonData.DebtorZipCode;
            this.DebtorCity = jsonData.DebtorCity;
            this.DebtorCountry = jsonData.DebtorCountry;
            this.DebtorEmail = jsonData.DebtorEmail;
            this.DebtorPhoneNumber = jsonData.DebtorPhoneNumber;
        }
    }
    
    DebtorFirstName: string;
    DebtorLastName: string;
    SupplierCustomerId: string;
    DebtorName: string;
    DebtorStreetname: string;
    DebtorHouseNumber: number;
    DebtorHouseNumberExtension: string;
    DebtorZipCode: string;
    DebtorCity: string;
    DebtorCountry: string;
    DebtorEmail: string;
    DebtorPhoneNumber: string;

}
