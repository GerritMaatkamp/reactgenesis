import { Subscription } from 'rxjs';
// Angular
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
// Layout
import { LayoutConfigService, SplashScreenService, TranslationService } from './core/_base/layout';
// language list
import { locale as nlLang } from './core/_config/i18n/nl';
import { locale as enLang } from './core/_config/i18n/en';
import { locale as chLang } from './core/_config/i18n/ch';
import { locale as esLang } from './core/_config/i18n/es';
import { locale as jpLang } from './core/_config/i18n/jp';
import { locale as deLang } from './core/_config/i18n/de';
import { locale as frLang } from './core/_config/i18n/fr';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
	// tslint:disable-next-line:component-selector
	selector: 'body[kt-root]',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit, OnDestroy {
	// Public properties
	title = 'Metronic';
	loader: boolean;
	private unsubscribe: Subscription[] = []; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

	/**
	 * Component constructor
	 *
	 * @param translationService: TranslationService
	 * @param router: Router
	 * @param layoutConfigService: LayoutCongifService
	 * @param splashScreenService: SplashScreenService
	 */
	constructor(
		private translationService: TranslationService,
		private router: Router,
		private layoutConfigService: LayoutConfigService,
		private splashScreenService: SplashScreenService,
		private iconRegistry: MatIconRegistry,
	    private sanitizer: DomSanitizer
			) {

		 this.setupIconRegistry(iconRegistry, sanitizer);
		// register translations
		this.translationService.loadTranslations(nlLang, enLang, chLang, esLang, jpLang, deLang, frLang);
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */

	ngOnInit(): void {
		// enable/disable loader
		this.loader = this.layoutConfigService.getConfig('loader.enabled');

		const routerSubscription = this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				// hide splash screen
				this.splashScreenService.hide();

				// scroll to top on every route change
				window.scrollTo(0, 0);

				// to display back the body content
				setTimeout(() => {
					document.body.classList.add('kt-page--loaded');
				}, 500);
			}
		});
		this.unsubscribe.push(routerSubscription);
	}

	setupIconRegistry(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
		// general
		iconRegistry.addSvgIcon('icon_settings', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_cog.svg'));
		iconRegistry.addSvgIcon('icon_settings_bold', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_cog_bold.svg'));
		iconRegistry.addSvgIcon('icon_dashboard', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_dashboard.svg'));
		iconRegistry.addSvgIcon('icon_invoice_euro', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_invoice_euro.svg'));
		iconRegistry.addSvgIcon('icon_edit', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_edit.svg'));
		iconRegistry.addSvgIcon('icon_envelope', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_envelope.svg'));
		iconRegistry.addSvgIcon('icon_brand', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_brand.svg'));
		iconRegistry.addSvgIcon('icon_communicate', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_communicate.svg'));
		iconRegistry.addSvgIcon('icon_users_bold', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_users_bold.svg'));

		// overview
		iconRegistry.addSvgIcon('icon_envelope_bold', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_envelope_bold.svg'));
		iconRegistry.addSvgIcon('icon_preview_bold', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_preview_bold.svg'));
		iconRegistry.addSvgIcon('icon_preview', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_preview.svg'));
		iconRegistry.addSvgIcon('icon_preview_text', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_preview_text.svg'));
		iconRegistry.addSvgIcon('icon_edit_bold', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_edit_bold.svg'));
		iconRegistry.addSvgIcon('icon_archive', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_archive.svg'));
		iconRegistry.addSvgIcon('icon_add_template', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_add_template.svg'));

		// make invoice
		iconRegistry.addSvgIcon('icon_userDB', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_userDB.svg'));
		iconRegistry.addSvgIcon('icon_userDB_bold', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_userDB_bold.svg'));
		iconRegistry.addSvgIcon('icon_userAdd', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_userAdd.svg'));

		iconRegistry.addSvgIcon('icon_xls', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_xls.svg'));
		iconRegistry.addSvgIcon('icon_pdf', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_pdf.svg'));
		iconRegistry.addSvgIcon('icon_worddoc', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_worddoc.svg'));
		
		iconRegistry.addSvgIcon('icon_home_light', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_home_light.svg'));
		iconRegistry.addSvgIcon('icon_phone', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_phone.svg'));
		iconRegistry.addSvgIcon('icon_url', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_url.svg'));
		iconRegistry.addSvgIcon('icon_png', sanitizer.bypassSecurityTrustResourceUrl('./assets/media/icons/svg/tool2pay/icon_png.svg'));
	}

	/**
	 * On Destroy
	 */
	ngOnDestroy() {
		this.unsubscribe.forEach(sb => sb.unsubscribe());
	}
}
