export class MenuConfig {
	public defaults: any = {
		header: {
			self: {},
			items: [
		
			]
		},
		aside: {
			self: {},
			items: [
				{
					title: 'Dashboard',
					root: true,
					icon: 'icon_dashboard',
					page: 'dashboard',
					translate: 'MENU.DASHBOARD',
				},
				{
					title: 'Invoices',
					root: true,
					icon: 'icon_invoice_euro',
					translate: 'MENU.INVOICES',
					bullet: 'dot',
					submenu: [
						{
							title: 'Overview',
							page: 'invoices/overview',
							bullet: 'dot',
							translate: 'MENU.INVOICES_OVERVIEW'
						}, {
							title: 'New',
							page: 'invoices/new',
							bullet: 'dot',
							translate: 'MENU.INVOICES_NEW'
						}, {
							title: 'Import',
							page: 'invoices/import',
							bullet: 'dot',
							translate: 'MENU.INVOICES_IMPORT'
						}, {
							title: 'Reminders',
							page: 'invoices/reminders',
							bullet: 'dot',
							translate: 'MENU.INVOICES_REMINDER'
						},
					]
				},
				{
					title: 'Customers',
					bullet: 'dot',
					icon: 'icon_userDB_bold',
					root: true,
					permission: 'accessToECommerceModule',
					translate: 'MENU.USERMAN',
					submenu: [
						{
							title: 'Overview',
							page: 'ecommerce/customers',
							translate: 'MENU.USERMAN',
						},
						{
							title: 'New customer',
							page: 'ecommerce/New customer',
							translate: 'MENU.USERMAN',
						},
						{
							title: 'Import customers',
							page: 'ecommerce/import',
							translate: 'MENU.USERMAN',
						},
					]
				},
				{
					section: 'Settings',
					translate: 'SETTINGS.SETTINGS'
				},
				{
					title: 'General',
					page: 'settings/general',
					icon: 'icon_settings_bold',
					translate: 'SETTINGS.GENERAL.PAGETITLE',
				},
				{
					title: 'User Management',
					root: true,
					bullet: 'dot',
					icon: 'icon_users_bold',
					translate: 'SETTINGS.USERMAN',
					submenu: [
						{
							title: 'Users',
							translate: 'SETTINGS.USERS',
							page: 'user-management/users'
						},
						{
							title: 'Roles',
							translate: 'SETTINGS.ROLES',
							page: 'user-management/roles'
						}
					]
				},
				{
					title: 'Communication',
					translate: 'MENU.COMMUNICATION',
					root: true,
					bullet: 'dot',
					icon: 'icon_communicate',
					submenu: [
						{
							title: 'E-mail',
							page: 'user-management/roles',
							translate: 'MENU.EMAIL',
						},
						{
							title: 'Email Templates',
							translate: 'MENU.TEMPLATES',
							page: 'settings/templates'
						},
					]
				},
				{
					title: 'Brand',
					page: 'settings/brand',
					icon: 'icon_brand',
					translate: 'SETTINGS.BRAND.PAGETITLE',
				},
			]
		},
	};

	public get configs(): any {
		return this.defaults;
	}
}
